#!/bin/bash

echo "#######################################################"
echo "#############Atualizando repositórios##################"
echo "#######################################################"

if ! apt update
then
	echo "Não foi possível atualizar os repositórios. Verifique seu arquivo /etc/apt/sources.list"
	exit 1
fi

echo "#######################################################"
echo "#######Iniciando remoção de aplicativos inúteis########"
echo "#######################################################"

if which hexchat pidgin
then
	apt remove hexchat pidgin -y
fi

echo "#######################################################"
echo "#######Iniciando instalação de aplicativos úteis#######"
echo "#######################################################"

if ! which htop vim git
then
	apt install htop vim git -y
fi

echo "#######################################################"
echo "########Iniciado a instalação do Oracle JDK############"
echo "#######################################################"

if ! java -version
then 
	add-apt-repository ppa:webupd8team/java -y && apt update
	apt install -y oracle-java8-installer
fi
exit 1
